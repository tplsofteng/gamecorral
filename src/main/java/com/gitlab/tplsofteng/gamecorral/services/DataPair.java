package com.gitlab.tplsofteng.gamecorral.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="DataPair")
@XmlAccessorType(XmlAccessType.PUBLIC_MEMBER)
public class DataPair
{
    
    private String key;
    
    
    private String value;

    /**
     * @return the key
     */
    @XmlAttribute
    public String getKey() {
        return key;
    }

    /**
     * @param key the key to set
     */
    public void setKey(String key) {
        this.key = key;
    }

    /**
     * @return the value
     */
    @XmlAttribute
    public String getValue() {
        return value;
    }

    /**
     * @param value the value to set
     */
    public void setValue(String value) {
        this.value = value;
    }
    
}