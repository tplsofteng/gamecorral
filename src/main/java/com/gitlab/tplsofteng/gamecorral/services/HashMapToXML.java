/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gitlab.tplsofteng.gamecorral.services;

import java.util.HashMap;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

/**
 *
 * @author tplsofteng
 */
public class HashMapToXML 
{
    private HashMap<String,String> map;

    /**
     * @return the map
     */
    public HashMapToXML(HashMap<String,String> map)
    {
        this.map = map;
    }
    
    @XmlJavaTypeAdapter(HashMapAdapter.class)
    public HashMap<String,String> getMap() {
        return map;
    }

    /**
     * @param map the map to set
     */
    public void setMap(HashMap<String,String> map) {
        this.map = map;
    }
}
