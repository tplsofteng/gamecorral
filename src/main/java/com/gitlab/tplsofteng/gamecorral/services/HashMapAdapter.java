/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gitlab.tplsofteng.gamecorral.services;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;
import javax.xml.bind.annotation.adapters.XmlAdapter;

/**
 *
 * @author tplsofteng
 */
public class HashMapAdapter extends XmlAdapter<JaxBHashmap, HashMap<String,String>>
{

    @Override
    public HashMap<String, String> unmarshal(JaxBHashmap v) throws Exception 
    {
        ArrayList<DataPair> pairs = new ArrayList<DataPair>(v.getPairs());
        HashMap<String,String> map = new HashMap<String,String>();
        for(int i = 0 ;i < pairs.size();i++)
        {
            map.put(pairs.get(i).getKey(), pairs.get(i).getValue());
        }
        
        return map;
    }

    @Override
    public JaxBHashmap marshal(HashMap<String, String> v) throws Exception 
    {
        JaxBHashmap map = new JaxBHashmap();
        for(Entry<String, String> entry : v.entrySet()) 
        {
            DataPair pair = new DataPair();
            pair.setKey(entry.getKey());
            pair.setValue(entry.getValue());
            map.addPair(pair);
        }
        return map;
    }

    
}
