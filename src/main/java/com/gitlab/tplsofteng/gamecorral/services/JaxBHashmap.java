/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gitlab.tplsofteng.gamecorral.services;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author tplsofteng
 */
@XmlRootElement(name="JaxBHashMap")
public class JaxBHashmap 
{
    private List<DataPair> pairs = new ArrayList<DataPair>();

    /**
     * @return the pairs
     */
    public List<DataPair> getPairs() {
        return pairs;
    }

    /**
     * @param pairs the pairs to set
     */
    public void setPairs(List<DataPair> pairs) {
        this.pairs = pairs;
    }
    
    public void addPair(DataPair pair)
    {
        pairs.add(pair);
    }
    
    public void removePair(DataPair pair)
    {
        pairs.remove(pair);
    }
    
    public void addPair(String Key, String Value)
    {
        DataPair p = new DataPair();
        p.setKey(Key);
        p.setValue(Value);
        pairs.add(p);
    }
    
    public void removePair(String Key, String Value)
    {
        DataPair p = new DataPair();
        p.setKey(Key);
        p.setValue(Value);
        pairs.remove(p);
    }
}
