/*
 * To change this template, choose Tools | Templates and open the template in
 * the editor.
 */
package com.gitlab.tplsofteng.gamecorral.gameAPI;

import javax.xml.bind.annotation.XmlRootElement;
import com.gitlab.tplsofteng.gamecorral.services.JaxBHashmap;

/**
 *
 * @author tplsofteng@gmail.com
 */
@XmlRootElement
public class DataRequest 
{
    public DataRequest()
    {
        gameName = "pingpong";
        requestType = "score";
        payload = new JaxBHashmap();
        payload.addPair("TEST-KEY", "TEST-VAL");
    }
    
    public String gameName;
    public String requestType;
    public JaxBHashmap payload;  
}
