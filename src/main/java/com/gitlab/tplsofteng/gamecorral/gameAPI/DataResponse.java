/*
 * To change this template, choose Tools | Templates and open the template in
 * the editor.
 */
package gameAPI;

import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author tplsofteng@gmail.com
 */
@XmlRootElement(name="DataResponse")
public class DataResponse 
{

    private String data = "NO DATA";
    
    public DataResponse()
    {
        data = "NO DATA";
    }
    
    public DataResponse(String data)
    {
        this.data = data;
    }
    
    @Override
    public String toString() 
    {
        return getData();
    }

    /**
     * @return the data
     */
    public String getData() {
        return data;
    }

    /**
     * @param data the data to set
     */
    public void setData(String data) {
        this.data = data;
    }
    
}
