
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package com.gitlab.tplsofteng.gamecorral.persistence;

//~--- non-JDK imports --------------------------------------------------------

import java.sql.*;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.transaction.SystemException;
import com.gitlab.tplsofteng.gamecorral.Entities.GameSiteUser;

/**
 *
 * @author tplsofteng@gmail.com
 */
public class tempCrudServiceBean implements CrudService {
    private Connection conn;

    public tempCrudServiceBean() {
        conn = getConnection();
    }

    @Override
    public Object create(Object t) throws SystemException {
        if (conn != null) {
            if (t.getClass() == GameSiteUser.class) {
                Statement st = null;

                try {
                    GameSiteUser user = (GameSiteUser) t;
                    String       pass = user.getPassword();
                    String       name = user.getUserName();
                    long         id   = user.getUserId();

                    st = conn.createStatement();
                    st.executeUpdate("INSERT INTO gameSiteUser(name,pass)" + "VALUES(" + "'" + name + "'" + "," + "'"
                                     + pass + "'" + ")");

                    ResultSet rs = st.getResultSet();

                    if ((rs != null) && rs.rowInserted()) {
                        System.out.println("xxxx" + rs.toString());
                    }
                } catch (SQLException ex) {
                    Logger.getLogger(tempCrudServiceBean.class.getName()).log(Level.SEVERE, null, ex);
                } finally {
                    if (st != null) {
                        try {
                            st.close();
                        } catch (SQLException ex) {
                            Logger.getLogger(tempCrudServiceBean.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                }
            }
        } else {
            conn = getConnection();
        }

        return t;
    }

    @Override
    public Object find(Class type, Object id) {
        GameSiteUser foundUser = null;

        if (conn != null) {
            Statement st = null;

            try {
                st = conn.createStatement();

                ResultSet rs = st.executeQuery("SELECT name,password FROM gameSiteUser WHERE id=" + "'" + id + "'");

                while (rs.next()) {
                    foundUser = new GameSiteUser();
                    foundUser.setId((Integer) id);
                    foundUser.setUserName(rs.getString("name"));
                    foundUser.setPassword(rs.getString("pass"));
                }
            } catch (SQLException ex) {
                Logger.getLogger(tempCrudServiceBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            conn = getConnection();
        }

        return foundUser;
    }

    @Override
    public Object update(Object t) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void delete(Class type, Object id) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List findWithNamedQuery(String queryName) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List findWithNamedQuery(String queryName, int resultLimit) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List findWithNamedQuery(String namedQueryName, Map parameters) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List findWithNamedQuery(String namedQueryName, Map parameters, int resultLimit) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public Long findUserIdByName(String userName) {
        long l = -1;

        if (conn != null) {
            Statement st = null;

            try {
                st = conn.createStatement();

                ResultSet rs = st.executeQuery("SELECT id FROM gameSiteUser WHERE name=" + "'" + userName + "'");

                System.out.println("xxxx" + rs.toString());

                while (rs.next()) {
                    l = rs.getLong("id");
                }
            } catch (SQLException ex) {
                Logger.getLogger(tempCrudServiceBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            conn = getConnection();
        }

        return l;
    }

    public Connection getConnection() {
        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(tempCrudServiceBean.class.getName()).log(Level.SEVERE, null, ex);
        }

        Connection conn                 = null;
        Properties connectionProperties = new Properties();

        connectionProperties.put("user", "gameSite");
        connectionProperties.put("password", "gameSite");

        try {
            conn = DriverManager.getConnection("jdbc:postgresql://127.0.0.1:5432/gameSite", connectionProperties);
        } catch (SQLException ex) {
            Logger.getLogger(tempCrudServiceBean.class.getName()).log(Level.SEVERE, null, ex);
        }

        return conn;
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
