package com.gitlab.tplsofteng.gamecorral.persistence;

//~--- JDK imports ------------------------------------------------------------

import java.util.List;
import java.util.Map;
import javax.transaction.SystemException;

/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */

/**
 *
 * @author tplsofteng@gmail.com 
 */
public interface CrudService {
    public Object create(Object t) throws SystemException;

    public Object find(Class type, Object id);

    public Object update(Object t);

    public void delete(Class type, Object id);

    public List findWithNamedQuery(String queryName);

    public List findWithNamedQuery(String queryName, int resultLimit);

    public List findWithNamedQuery(String namedQueryName, Map parameters);

    public List findWithNamedQuery(String namedQueryName, Map parameters, int resultLimit);

    public java.lang.Long findUserIdByName(java.lang.String userName);
}


//~ Formatted by Jindent --- http://www.jindent.com
