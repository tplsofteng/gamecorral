package com.gitlab.tplsofteng.gamecorral.persistence;

//~--- JDK imports ------------------------------------------------------------

import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import javax.persistence.Persistence;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */

/**
 *
 * @author tplsofteng@gmail.com 
 */
public class CrudServiceBean implements CrudService {
    EntityManager           em;
    EntityManagerFactory    emf;
    @Resource
    private UserTransaction utx;

    public CrudServiceBean() {
        emf = Persistence.createEntityManagerFactory("GameSitePU");
        em  = emf.createEntityManager();
    }

    @Override
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public Object create(Object t) throws SystemException {
        em.getTransaction().begin();
        this.em.persist(t);
        em.getTransaction().commit();

        return t;
    }

    @Override
    public Object find(Class type, Object id) {
        return this.em.find(type, id);
    }

    @Override
    public Object update(Object t) {
        
        em.getTransaction().begin();
        t = (Object) this.em.merge(t);
        em.getTransaction().commit();
        
        return t;
    }

    @Override
    public void delete(Class type, Object id) {
        Object ref = this.em.getReference(type, id);
        this.em.remove(ref);
    }

    @Override
    public Long findUserIdByName(String userName) {
        try 
        {
            return (Long) em.createNamedQuery("findUserIdByName").setParameter("userName", userName).getSingleResult();
        } 
        catch (NoResultException e) 
        {
            return -1l;
        }
    }

    public Long findGameIdByName(String gameTitle) {
        try 
        {
            return (Long) em.createNamedQuery("findGameIdByTitle").setParameter("title", gameTitle).getSingleResult();
        } 
        
        catch (NoResultException e) {
            return -1l;
        }
    }

    @Override
    public List findWithNamedQuery(String queryName) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List findWithNamedQuery(String queryName, int resultLimit) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List findWithNamedQuery(String namedQueryName, Map parameters) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List findWithNamedQuery(String namedQueryName, Map parameters, int resultLimit) {
        throw new UnsupportedOperationException("Not supported yet.");
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
