
/*
* To change this template, choose Tools | Templates and open the template in
* the editor.
 */
package com.gitlab.tplsofteng.gamecorral.appwide;

//~--- non-JDK imports --------------------------------------------------------

import java.util.HashMap;
import com.gitlab.tplsofteng.gamecorral.Entities.GamePackage;

/**
 *
 * @author tplsofteng@gmail.com
 */
public class GameManager {
    private GameManager                  instance;
    private HashMap<String, GamePackage> loadedGames;

    private GameManager() {}

    public GameManager getInstance() {
        if (instance == null) {
            instance = new GameManager();
        }

        return this.instance;
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
