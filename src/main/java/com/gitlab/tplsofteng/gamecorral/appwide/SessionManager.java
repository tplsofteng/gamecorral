package com.gitlab.tplsofteng.gamecorral.appwide;

//~--- non-JDK imports --------------------------------------------------------

import java.io.Serializable;
import java.util.HashMap;
import com.gitlab.tplsofteng.gamecorral.Entities.GameSiteUser;

/*
* To change this template, choose Tools | Templates and open the template in
* the editor.
 */

/**
 *
 * @author tplsofteng@gmail.com
 */
public class SessionManager implements Serializable {
    private static SessionManager         instance;
    private HashMap<String, GameSiteUser> Sessions;

    private SessionManager() {
        Sessions = new HashMap<String, GameSiteUser>(50);
    }

    public static SessionManager getInstance() {
        if (instance == null) {
            instance = new SessionManager();
        }

        return instance;
    }

    public void createSession(GameSiteUser user, String sessionId) {
        Sessions.put(sessionId, user);
    }

    public GameSiteUser getUserForSessionId(String sessionId) {
        GameSiteUser user = Sessions.get(sessionId);

        return user;
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
