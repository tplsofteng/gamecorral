package com.gitlab.tplsofteng.gamecorral.backing;

//~--- non-JDK imports --------------------------------------------------------

/*
* To change this template, choose Tools | Templates and open the template in
* the editor.
 */
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.transaction.SystemException;
import com.gitlab.tplsofteng.gamecorral.Entities.GameSiteUser;
import com.gitlab.tplsofteng.gamecorral.persistence.CrudServiceBean;

/**
 *
 * @author tplsofteng@gmail.com
 */
@ManagedBean(name = "SignupBean")
@RequestScoped
public class SignupBean {
    private CrudServiceBean dao;
    private String          password;

    /**
     * Creates a new instance of SignupBean
     */
    private String username;

    public SignupBean() {
        dao = new CrudServiceBean();
    }

    public void signup() {
        System.err.println("SIGNUP");

        FacesMessage msg     = null;
        GameSiteUser newUser = new GameSiteUser();

        newUser.setUserName(username);
        newUser.setPassword(password);

        if (dao.findUserIdByName(username) == -1) {
            try {
                dao.create(newUser);

                if (dao.findUserIdByName(newUser.getUserName()) != -1) {
                    msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Signup Success", username + " Account Created");
                } else {
                    msg = new FacesMessage(FacesMessage.SEVERITY_WARN, "Signup Error",
                                           "Something went wrong, Your account may not have been created");
                }
            } catch (SystemException ex) {
                msg = new FacesMessage(FacesMessage.SEVERITY_WARN, "Signup Error",
                                       "An error has occurred, your account has not been created");
            }
        } else {
            msg = new FacesMessage(FacesMessage.SEVERITY_WARN, "Signup Error", "Sorry That name is not available");
        }

        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    /**
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * @param username the username to set
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
