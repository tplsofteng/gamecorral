package com.gitlab.tplsofteng.gamecorral.backing;

//~--- non-JDK imports --------------------------------------------------------

/*
* To change this template, choose Tools | Templates and open the template in
* the editor.
 */
import java.util.ArrayList;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import com.gitlab.tplsofteng.gamecorral.Entities.GamePackage;

/**
 *
 * @author tplsofteng@gmail.com
 */
@ManagedBean
@RequestScoped
public class HomePageBean extends GameSiteManagedBean {
    private ArrayList<GamePackage> trendingGames;

    /**
     * Creates a new instance of HomePageBean
     */
    public HomePageBean() {
        trendingGames = new ArrayList<GamePackage>(7);

        for (int i = 0; i < trendingGames.size(); i++) {
            GamePackage pk = new GamePackage();

            pk.setTitle("A game");
            trendingGames.add(pk);
        }
    }

    /**
     * @return the trendingGames
     */
    public ArrayList<GamePackage> getTrendingGames() {
        return trendingGames;
    }

    /**
     * @param trendingGames the trendingGames to set
     */
    public void setTrendingGames(ArrayList<GamePackage> trendingGames) {
        this.trendingGames = trendingGames;
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
