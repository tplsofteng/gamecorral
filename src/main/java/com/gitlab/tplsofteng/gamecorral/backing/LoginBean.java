package com.gitlab.tplsofteng.gamecorral.backing;

//~--- non-JDK imports --------------------------------------------------------

/*
* To change this template, choose Tools | Templates and open the template in
* the editor.
 */
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import org.primefaces.context.RequestContext;
import com.gitlab.tplsofteng.gamecorral.Entities.GameSiteUser;
import com.gitlab.tplsofteng.gamecorral.appwide.SessionManager;
import com.gitlab.tplsofteng.gamecorral.persistence.CrudServiceBean;

/**
 *
 * @author tplsofteng@gmail.com
 */
@ManagedBean(name = "LoginBean")
@RequestScoped
public class LoginBean extends GameSiteManagedBean {
    private CrudServiceBean        dao;
    private String                 password;
    private SessionManager         sessionmgr;
    private String                 username;

    /**
     * Creates a new instance of loginBean
     */
    public LoginBean() 
    {
        dao        = new CrudServiceBean();
        sessionmgr = SessionManager.getInstance();
    }

    public void login() {
        System.err.println("LOGIN!!");

        RequestContext requestContext = RequestContext.getCurrentInstance();
        boolean        loggedIn       = false;

        if (username != null) {
            if (password != null) {
                GameSiteUser user = new GameSiteUser();

                user.setUserName(username);
                user.setPassword(password);

                long userID = -1;

                userID = dao.findUserIdByName(username);
                System.err.println("ID" + userID);

                if (userID != -1) {
                    user = (GameSiteUser) dao.find(user.getClass(), userID);

                    if (user == null) {
                        System.err.println("ID" + userID);
                        facesError("Login Error", "Not Found");
                    }

                    // The password and username check out = successful login
                    else if (user.getPassword().equals(password)) {
                        loggedIn = true;
                        facesInfo("Welcome", username);

                        String sessionId = readSessionId();

                        sessionmgr.createSession(user, sessionId);
                    }
                } else {
                    facesError("Login Error", "Not Found");
                }
            } else {
                facesError("Login Error", "Please Enter a password");
            }
        } else {
            facesError("Login Error", "please enter a user name");
        }

        requestContext.addCallbackParam("loggedIn", loggedIn);
    }

    public void logout() {}

    /**
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * @param username the username to set
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
