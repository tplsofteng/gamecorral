package com.gitlab.tplsofteng.gamecorral.backing;

//~--- non-JDK imports --------------------------------------------------------

/*
* To change this template, choose Tools | Templates and open the template in
* the editor.
 */
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.Cookie;
import javax.transaction.SystemException;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.FlowEvent;
import org.primefaces.model.UploadedFile;
import com.gitlab.tplsofteng.gamecorral.Entities.GamePackage;
import com.gitlab.tplsofteng.gamecorral.Entities.GameSiteUser;
import com.gitlab.tplsofteng.gamecorral.appwide.SessionManager;
import com.gitlab.tplsofteng.gamecorral.persistence.CrudServiceBean;

/**
 *
 * @author tplsofteng@gmail.com
 */
@ManagedBean
@SessionScoped
public class profilePageBean extends GameSiteManagedBean {
    private int                points = -1;
    private List<GamePackage>  Games;
    private GamePackage        currentGame;
    private CrudServiceBean    dao;
    private List<GameSiteUser> friends;
    private boolean            gameUploadSkip;
    private String             password;
    SessionManager     sessionMgr;
    GameSiteUser               user;
    private String             userName;

    public profilePageBean() {
        super();
        sessionMgr     = SessionManager.getInstance();
        currentGame    = new GamePackage();
        gameUploadSkip = false;
    }

    public void initialize() {
        setDao(new CrudServiceBean());

        GameSiteUser temp = sessionMgr.getUserForSessionId(readSessionId());
        FacesMessage msg;

        if (temp == null) 
        {
            msg = new FacesMessage(FacesMessage.SEVERITY_WARN, "Session Error", "Not Found");
            FacesContext.getCurrentInstance().addMessage(null, msg);
        } 
        
        else 
        {
            this.user     = temp;
            this.password = temp.getPassword();
            this.points   = temp.getPoints();
            this.userName = temp.getUserName();
            this.setGames(temp.getGames());
            this.setFriends(temp.getFriends());
            msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Welcome", this.userName);
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }
    }

    /**
     * @return the dao
     */
    public CrudServiceBean getDao() {
        return dao;
    }

    /**
     * @param dao the dao to set
     */
    public void setDao(CrudServiceBean dao) {
        this.dao = dao;
    }

    /**
     * @return the userName
     */
    public String getUserName() {
        return userName;
    }

    /**
     * @param userName the userName to set
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }

    /**
     * @return the points
     */
    public int getPoints() {
        return points;
    }

    /**
     * @param points the points to set
     */
    public void setPoints(int points) {
        this.points = points;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @return the friends
     */
    public List<GameSiteUser> getFriends() {
        return friends;
    }

    /**
     * @param friends the friends to set
     */
    public void setFriends(List<GameSiteUser> friends) {
        this.friends = friends;
    }

    /**
     * @return the Games
     */
    public List<GamePackage> getGames() {
        if (!Games.isEmpty()) {
            System.err.println("Games Requested " + Games);
        }

        return Games;
    }

    /**
     * @param Games the Games to set
     */
    public void setGames(List<GamePackage> Games) {
        this.Games = Games;
    }

    public void addGame(GamePackage newGame) {
        this.Games.add(newGame);
    }

    public void handleGameUpload(FileUploadEvent evt) {
        FacesMessage msg;

        msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Got it", "Game Uploaded Successfully");

        UploadedFile gameFile = evt.getFile();
        GamePackage  g        = new GamePackage();

        g.setGameFile(gameFile.getContents());
        g.setTitle(currentGame.getTitle());
        System.err.println("GAME: " + currentGame.getTitle());
        g.setUploader(user);
        addGame(g);

        try 
        {
            dao.create(g);
        } 
        
        catch (SystemException ex) 
        {
            msg = new FacesMessage(FacesMessage.SEVERITY_WARN, "Saving Game Error", "Failed to save game");
        }

        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    public String onGameUploadFlowProcess(FlowEvent evt) {
        if (gameUploadSkip) {
            gameUploadSkip = false;

            return "confirm";
        } 
        
        else 
        {
            return evt.getNewStep();
        }
    }

    /**
     * @return the uploadedGame
     */
    public GamePackage getCurrentGame() {
        return currentGame;
    }

    /**
     * @param uploadedGame the uploadedGame to set
     */
    public void setCurrentGame(GamePackage uploadedGame) {
        this.currentGame = uploadedGame;
    }

    /**
     * @return the gameUploadSkip
     */
    public boolean isGameUploadSkip() {
        return gameUploadSkip;
    }

    /**
     * @param gameUploadSkip the gameUploadSkip to set
     */
    public void setGameUploadSkip(boolean gameUploadSkip) {
        this.gameUploadSkip = gameUploadSkip;
    }

    public void refresh() {
        if (isRefreshGames()) {
            this.user = (GameSiteUser) dao.find(user.getClass(), user.getUserId());
        }
    }

    /**
     * @return the refreshGames
     */
    public boolean isRefreshGames() {
        Cookie refreshCookie = readCookie("gameSiteRefresh");

        if ((refreshCookie != null) && refreshCookie.getValue().equals("true")) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @param refreshGames the refreshGames to set
     */
    public static void setRefreshGames(boolean refreshGames) {
        Cookie refreshCookie;

        if (refreshGames) {
            refreshCookie = new Cookie("gameSiteRefresh", "true");
        } else {
            refreshCookie = new Cookie("gameSiteRefresh", "false");
        }

        writeCookie(refreshCookie);
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
