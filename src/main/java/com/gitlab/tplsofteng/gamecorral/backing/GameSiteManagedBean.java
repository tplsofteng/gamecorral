package com.gitlab.tplsofteng.gamecorral.backing;

//~--- non-JDK imports --------------------------------------------------------

import java.io.Serializable;
import java.util.Map;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import com.gitlab.tplsofteng.gamecorral.Entities.GameSiteUser;
import com.gitlab.tplsofteng.gamecorral.appwide.SessionManager;

/*
* To change this template, choose Tools | Templates and open the template in
* the editor.
 */

/**
 *
 * @author tplsofteng@gmail.com
 */
public class GameSiteManagedBean implements Serializable {
    protected SessionManager SessionMgr = SessionManager.getInstance();
    protected boolean        reloadNeeded;

    public GameSiteManagedBean() {
        SessionMgr = SessionManager.getInstance();
    }
    
    public String getActiveUserName() {
        String name = getActiveUser().getUserName();
        return name;
    }

    public GameSiteUser getActiveUser() {
        GameSiteUser user = SessionMgr.getUserForSessionId(readSessionId());
        return user;
    }

    public boolean isLoggedIn() 
    {
        boolean ans     = false;
        Cookie  session = readCookie("JSESSIONID");

        if (session != null) 
        {
            GameSiteUser user = SessionMgr.getUserForSessionId(session.getValue());

            if (user != null) 
            {
                ans = true;
            }
        }

        return ans;
    }

    // Convenience Method to read the current session cookie managed by the server
    public String readSessionId() {
        Cookie session = readCookie("JSESSIONID");

        return session.getValue();
    }

    public static Cookie readCookie(String name) {
        FacesContext        context = FacesContext.getCurrentInstance();
        Map<String, Object> cookies = context.getExternalContext().getRequestCookieMap();
        Cookie              cookie  = null;

        if (cookies.containsKey(name)) {
            cookie = (Cookie) cookies.get(name);
        }

        return cookie;
    }

    public static void writeCookie(Cookie cookie) {

        // Add a cookie to the browser
        FacesContext context = FacesContext.getCurrentInstance();

        ((HttpServletResponse) context.getExternalContext().getResponse()).addCookie(cookie);
    }
    
    public static void facesInfo(String message)
    {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(message));
    }
    
    public static void facesInfo(String summary, String detail)
    {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(summary, detail));
    }
    
    public static void facesWarn(String message)
    {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN ,"Message", message));
    }
    
    public static void facesWarn(String summary, String detail)
    {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN ,summary, detail));
    }
    
    public static void facesError(String message)
    {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR ,"Message", message));
    }
    
    public static void facesError(String summary, String detail)
    {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR ,summary, detail));
    }

    /**
     * @return the reloadNeeded
     */
    public boolean isReloadNeeded() {
        return reloadNeeded;
    }

    /**
     * @param reloadNeeded the reloadNeeded to set
     */
    public void setReloadNeeded(boolean reloadNeeded) {
        this.reloadNeeded = reloadNeeded;
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
