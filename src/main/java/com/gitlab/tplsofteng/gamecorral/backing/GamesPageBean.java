/*
 * To change this template, choose Tools | Templates and open the template in
 * the editor.
 */
package com.gitlab.tplsofteng.gamecorral.backing;

import java.util.ArrayList;
import javax.annotation.ManagedBean;
import com.gitlab.tplsofteng.gamecorral.Entities.GamePackage;
import com.gitlab.tplsofteng.gamecorral.persistence.CrudServiceBean;

/**
 *
 * @author tplsofteng@gmail.com
 */
@ManagedBean
public class GamesPageBean extends GameSiteManagedBean 
{
    private ArrayList<GamePackage> topGames;
    private CrudServiceBean dao;
    
    public GamesPageBean()
    {
        topGames = new ArrayList<GamePackage>(7);
        dao = new CrudServiceBean();
        
    }

    /**
     * @return the topGames
     */
    public ArrayList<GamePackage> getTopGames() {
        return topGames;
    }

    /**
     * @param topGames the topGames to set
     */
    public void setTopGames(ArrayList<GamePackage> topGames) {
        this.topGames = topGames;
    }
}
