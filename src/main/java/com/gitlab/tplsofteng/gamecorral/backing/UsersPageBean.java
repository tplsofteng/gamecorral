/*
 * To change this template, choose Tools | Templates and open the template in
 * the editor.
 */
package com.gitlab.tplsofteng.gamecorral.backing;
import com.ocpsoft.pretty.faces.annotation.URLMapping;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import com.gitlab.tplsofteng.gamecorral.Entities.GameSiteUser;
import com.gitlab.tplsofteng.gamecorral.persistence.CrudServiceBean;

/**
 *
 * @author tplsofteng@gmail.com
 */
@ManagedBean
@RequestScoped
@URLMapping(
    id      = "usersPage",
    pattern = "/users/#{usersPageBean.queryName}",
    viewId  = "/usersPage.xhtml"
)

public class UsersPageBean extends GameSiteManagedBean
{
    private String queryName;
    private GameSiteUser user;
    private CrudServiceBean dao;

    
    public UsersPageBean()
    {
       
        super();
        dao = new CrudServiceBean();
    }
    

    /**
     * @return the userName
     */
    public String getQueryName() {
        return queryName;
    }

    /**
     * @param userName the userName to set
     */
    public void setQueryName(String userName) {
        this.queryName = userName;
        
        if(user == null)
        {
            long id = dao.findUserIdByName(queryName);
            user = (GameSiteUser)dao.find(GameSiteUser.class, id);
        }
        
    }

    /**
     * @return the friends
     */
    public List<GameSiteUser> getFriends() {
        return user.getFriends();
    }

    /**
     * @param friends the friends to set
     */
    public void setFriends(ArrayList<GameSiteUser> friends) {
        this.user.setFriends(friends);
    }

    /**
     * @return the userName
     */
    public String getUserName() 
    {
        if(user !=null)
        {
            return user.getUserName();
        }
        else
        {
            return "User Not Found";
        }
        
    }

    /**
     * @param userName the userName to set
     */
    public void setUserName(String userName) 
    {
        if(user!= null)
        {
            this.user.setUserName(userName);
        }
    }
    
    public boolean userExists()
    {
        if(user == null)
        {
            return false;
        }
        else
        {
            return true;
        }
    }
    
    public boolean isFriend()
    {
        GameSiteUser temp = getActiveUser();
        if (user.getFriends().contains(temp))
        {
            
            return true;
        }
        
        else
        {
            return false;
        }
    }
    
    public boolean isActiveUser()
    {
        GameSiteUser temp = getActiveUser();
        if (temp.getUserId() == user.getUserId())
        {
            
            return true;
        }
        
        else
        {
            return false;
        }
    }
    
    public void sendFriendRequest()
    {
        GameSiteUser activeUser = getActiveUser();
        if(activeUser != null)
        {
            if(activeUser.getUserId() != user.getUserId())
            {
                user.addFriendRequest(activeUser.getUserId());
                user = (GameSiteUser)dao.update(user);
                if(user.getFriends().size() > 0)
                {
                    System.err.println(user.getFriends().get(0));
                }
                
                else
                {
                    facesInfo("Something Went wrong the request did not reach the user");
                }
            }
            
            else
            {
                facesInfo("Can't friend yourself ^^");
            }
        }
        else
        {
            facesInfo("Couldn't find user, request not sent");
            
        }
    }
    
}
