package com.gitlab.tplsofteng.gamecorral.backing;

//~--- non-JDK imports --------------------------------------------------------

/*
* To change this template, choose Tools | Templates and open the template in
* the editor.
 */
import com.ocpsoft.pretty.faces.annotation.URLMapping;
import java.io.ByteArrayInputStream;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import com.gitlab.tplsofteng.gamecorral.Entities.GamePackage;
import com.gitlab.tplsofteng.gamecorral.persistence.CrudServiceBean;

/**
 *
 * @author tplsofteng@gmail.com
 */
@ManagedBean
@RequestScoped
@URLMapping(
    id      = "gamePage",
    pattern = "/games/#{ gamePageBean.gameTitle }",
    viewId  = "/gamePage.xhtml"
)
public class GamePageBean extends GameSiteManagedBean {
    private GamePackage     currentGame;
    private CrudServiceBean dao;

    /**
     * Creates a new instance of gamesPageBean
     */
    
    private String gameTitle;

    public GamePageBean() 
    {
        dao = new CrudServiceBean();
    }

    public String getGame() 
    {
        long id = getDao().findGameIdByName(getGameTitle());

        currentGame = (GamePackage) getDao().find(GamePackage.class, id);

        if (currentGame != null)
        {
            return currentGame.getTitle();
        }
        
        else 
        {
            return "Unknown Game";
        }
    }

    public String getContent() 
    {
        String script;

        if (currentGame != null) 
        {
            if (currentGame.getGameFile() != null) 
            {
                script = new String(currentGame.getGameFile());
            }
            
            else 
            {
                script = "No Content error";
            }
            
        } 
        else 
        {
            script = "Game Not Found";
        }

        System.err.println("Script: " + script);

        return script;
    }

    public StreamedContent getThumb() 
    {
        ByteArrayInputStream   imgStream = new ByteArrayInputStream(currentGame.getThumb());
        DefaultStreamedContent thumb     = new DefaultStreamedContent(imgStream, "image/png");

        return thumb;
    }

    /**
     * @return the gameTitle
     */
    public String getGameTitle() {
        return gameTitle;
    }

    /**
     * @param gameTitle the gameTitle to set
     */
    public void setGameTitle(String gameTitle) {
        this.gameTitle = gameTitle;
    }

    /**
     * @return the currentGame
     */
    public GamePackage getCurrentGame() {
        return currentGame;
    }

    /**
     * @param currentGame the currentGame to set
     */
    public void setCurrentGame(GamePackage currentGame) {
        this.currentGame = currentGame;
    }

    /**
     * @return the dao
     */
    public CrudServiceBean getDao() {
        return dao;
    }

    public String getPageContent() 
    {
        if (currentGame != null) 
        {
            return this.currentGame.getPageData();
        } 
        else 
        {
            return "Game Not Found ";
        }
    }

    /**
     * @param dao the dao to set
     */
    public void setDao(CrudServiceBean dao) {
        this.dao = dao;
    }

    protected void reloadBackingData() {
        throw new UnsupportedOperationException("Not supported yet.");
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
