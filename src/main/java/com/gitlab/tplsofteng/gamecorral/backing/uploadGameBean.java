/*
 * To change this template, choose Tools | Templates and open the template in
 * the editor.
 */
package com.gitlab.tplsofteng.gamecorral.backing;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.htmlcleaner.HtmlCleaner;
import org.htmlcleaner.SimpleHtmlSerializer;
import org.htmlcleaner.TagNode;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.FlowEvent;
import org.primefaces.model.UploadedFile;
import com.gitlab.tplsofteng.gamecorral.Entities.GamePackage;
import com.gitlab.tplsofteng.gamecorral.Entities.GameSiteUser;
import com.gitlab.tplsofteng.gamecorral.persistence.CrudServiceBean;


/**
 *
 * @author tplsofteng@gmail.com
 */

@ManagedBean
@ViewScoped
public class uploadGameBean extends GameSiteManagedBean
{
    private CrudServiceBean dao;
    private boolean gameUploadSkip = false;
    protected boolean zip;
    private GamePackage currentGame;
    private GameSiteUser user;
    private HtmlCleaner cleaner;
    private ArrayList<String> scriptPaths;
    private ArrayList<String> resPaths;
    /**
     * Creates a new instance of uploadGameBean
     */
    public uploadGameBean() 
    {
        super();
        currentGame = new GamePackage();
        dao = new CrudServiceBean();
        user = getActiveUser();
    }
    
    public void handleGameUpload(FileUploadEvent evt)
    {
        
        UploadedFile gameFile = evt.getFile();
        facesInfo("Got " + gameFile.getFileName(), "Game Uploaded Successfully");
        
        if(gameFile.getFileName().matches("^.*\\.zip"))
        {
            System.err.println("ZIPFILE");
            handleZipUpload2(gameFile);
        }
        currentGame.setUploader(user);
    }
    
    public void handleIconUpload(FileUploadEvent evt)
    {
        FacesMessage msg;
        msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Got it", "Game Icon Uploaded Successfully");
        UploadedFile picFile = evt.getFile();
        System.err.println("File Name " + picFile.getFileName());
        System.err.println("Got Pic for "  + currentGame.getTitle());
        
        if(picFile.getFileName().matches("^.*\\.(png|jpg|gif)"))
        {
            System.err.println("IMG");
            //handlePNGUpload(picFile);
            currentGame.setThumb(picFile.getContents());
            dao.update(currentGame);
        }
        
        else
        {
            currentGame.setThumb(picFile.getContents());
        }
        
        currentGame.setUploader(user);
        
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }
    
    public void commitUpload()
    {
        FacesMessage msg;
        
        System.err.println("Saving Game " + currentGame.getGameid());
        
        if(dao.findGameIdByName(currentGame.getTitle()) == -1)
        {
            try 
            {
                user.addGame(currentGame);
                dao.update(user);
                //dao.create(currentGame);

                if(dao.findGameIdByName(currentGame.getTitle()) != -1)
                {
                    msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Save Success", currentGame.getTitle() + " Created");
                }
                
                else
                {
                    msg = new FacesMessage(FacesMessage.SEVERITY_WARN, "Save Error", "Something went wrong, Your game may not have been created");
                }
                
                profilePageBean.setRefreshGames(true);
            } 

            catch (Exception ex) //System
            {
                msg = new FacesMessage(FacesMessage.SEVERITY_WARN, "Save Error", "An error has occurred, your game has not been created");
            }
        }
        else
        {
            msg = new FacesMessage(FacesMessage.SEVERITY_WARN, "Save Error", "Sorry That title name is not available");
        }
        
        System.err.println("Save Game: " + msg.getDetail());
        FacesContext.getCurrentInstance().addMessage(null, msg);  
        
    }

    /**
     * @return the dao
     */
    public CrudServiceBean getDao() {
        return dao;
    }

    /**
     * @param dao the dao to set
     */
    public void setDao(CrudServiceBean dao) {
        this.dao = dao;
    }

    /**
     * @return the gameUploadSkip
     */
    public boolean isGameUploadSkip() {
        return gameUploadSkip;
    }

    /**
     * @param gameUploadSkip the gameUploadSkip to set
     */
    public void setGameUploadSkip(boolean gameUploadSkip) {
        this.gameUploadSkip = gameUploadSkip;
    }

    /**
     * @return the user
     */
    public GameSiteUser getUser() {
        return user;
    }

    /**
     * @param user the user to set
     */
    public void setUser(GameSiteUser user) {
        this.user = user;
    }
    
    public String onGameUploadFlowProcess(FlowEvent evt)
    {
        String step = evt.getNewStep();

        if(gameUploadSkip)
        {
            gameUploadSkip = false;
            step = "confirm";  
        }
        
        return step;
        
    }

    /**
     * @return the currentGame
     */
    public GamePackage getCurrentGame() {
        return currentGame;
    }

    /**
     * @param currentGame the currentGame to set
     */
    public void setCurrentGame(GamePackage currentGame) {
        this.currentGame = currentGame;
    }

    /**
     * @return the title
     */
    public String getTitle() {
        return currentGame.getTitle();
    }

    /**
     * @param title the title to set
     */
    public void setTitle(String title) 
    {
        currentGame.setTitle(title);
    }

    private void handleZipUpload(UploadedFile gameFile) 
    {
        System.err.println("File is a" + gameFile.getContentType());
        byte[] zipFile = gameFile.getContents();
        
        ByteArrayInputStream bis = new ByteArrayInputStream(zipFile);
        ZipInputStream zin = new ZipInputStream(bis);
        
        HashMap<String,byte[]> zipEntries = new HashMap<String, byte[]>(30);
        ZipEntry entry;
        
        TagNode HTMLNodes = new TagNode("");
        try 
        {
            //Read all entries in the zip file
            while((entry = zin.getNextEntry()) !=null)
            {
                //If we find a file called index.html
                if(!entry.isDirectory() && entry.getName().matches(".*index.html"))
                {
                    //read the index file out of the entry
                    String rawIndex = new String(processZipEntry(zin));
                    cleaner = new HtmlCleaner();
                    
                    //get a traversable set of html node objects from our document
                    HTMLNodes = cleaner.clean(rawIndex);
                    System.err.println("DOC " + rawIndex);
                    
                    //Now get an array of all <script> tags in the document
                    TagNode[] scripts = HTMLNodes.getElementsByName("script", true);
                    
                    //get the paths to the script files referenced in the html
                    scriptPaths = new ArrayList<String>(5);
                    for(int n = 0; n < scripts.length;n++ )
                    {
                        scriptPaths.add(scripts[n].getAttributeByName("src"));
                    }
                    
                }

                else
                {
                    //just another file in the zip, could be anything
                    byte[] data = processZipEntry(zin);
                    zipEntries.put(entry.getName(), data);
                }
                
            }//End of zip processing
            
            
            //there is more in the zip than index.html
            if(!zipEntries.isEmpty())
            {
                //there are scripts in index.html and we have paths to them
                if(!scriptPaths.isEmpty())
                {
                    //loop all script paths
                    for(int i =0; i < scriptPaths.size(); i++)
                    {
                        System.err.println("Script at " + scriptPaths.get(i));
                        
                        //loop the other files in the zip to see if there are any matching paths
                        for (Map.Entry<String, byte[]> z : zipEntries.entrySet())
                        {
                            System.err.println("Entry " + z.getKey());
                            
                            //if a pattern matches we have found the script referenced in html, now make it inline
                            if(z.getKey().matches(".*" + scriptPaths.get(i)))
                            {
                                //find the script that matches the path we have for it and remove the src= bit
                                TagNode matchedScript = HTMLNodes.findElementByAttValue("src", scriptPaths.get(i), true, true);
                                matchedScript.removeAttribute("src");
                                
                                String content = new String(z.getValue());
                                cleaner.setInnerHtml(matchedScript, content);
                                
                                SimpleHtmlSerializer serializer = new SimpleHtmlSerializer(cleaner.getProperties());

                                currentGame.setPageData(serializer.getAsString(HTMLNodes));
                            }
                         }
                     }
                        
                  }
 
               }
            
        }

        catch (IOException ex) 
        {
            Logger.getLogger(uploadGameBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
        
        byte[] decompressedData = new byte[50];
        currentGame.setGameFile(decompressedData);
    }
    
    private void handlePNGUpload(UploadedFile picFile) 
    {
        
    }

    private byte[] processZipEntry(ZipInputStream zin) throws IOException 
    {
        int buffSize = 2048;
        byte[] pageBuffer = new byte[buffSize];
        byte[] pageBytes = new byte[0];

        for (int c = 0; c != -1; c = zin.read(pageBuffer)) 
        {
            byte[] pageBytesTemp = new byte[pageBytes.length + c];
            //Copy page so far into enlarged temp array
            System.arraycopy(pageBytes, 0, pageBytesTemp, 0, pageBytes.length);

            //add buffer to enlarged array
            System.arraycopy(pageBuffer, 0, pageBytesTemp, pageBytes.length , c);

            //replace the old page with the new enlarged array
            pageBytes = pageBytesTemp;
        }

        return pageBytes;
    }

    private void processJavaScript(String content) 
    {
        int audioPos = content.indexOf("Audio(", 0) + 7;
        if(audioPos != -1)
        {
            StringBuilder audioPath = new StringBuilder(100);
            int count = audioPos;
            char buff = content.charAt(audioPos);
            while(buff != '"' && count - audioPos < 100)
            {
                buff = content.charAt(count);
                if(buff != '"')
                {
                    audioPath.append(buff);
                }
                count++;
            }

            System.out.println("AudioPath " + audioPath.toString());
        }
    }

    private void processScripts() 
    {
        
    }
}
