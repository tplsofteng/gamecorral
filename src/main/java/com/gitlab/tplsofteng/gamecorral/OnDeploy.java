package com.gitlab.tplsofteng.gamecorral;


import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.transaction.SystemException;
import com.gitlab.tplsofteng.gamecorral.Entities.GameSiteUser;
import com.gitlab.tplsofteng.gamecorral.persistence.CrudServiceBean;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author tplsofteng@gmail.com
 */
public class OnDeploy implements ServletContextListener
{
    private CrudServiceBean dao;
    
    public OnDeploy()
    {
        
    }
    
    @Override
    public void contextInitialized(ServletContextEvent sce) {
        System.out.println("Init APP");
        dao = new CrudServiceBean();
        GameSiteUser user = new GameSiteUser();
        user.setUserName("tplsofteng");
        user.setPassword("f4getcreator");
        
        try 
        {
            dao.create(user);
        } 
        catch (SystemException ex) 
        {
            Logger.getLogger(OnDeploy.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        System.out.println("CLOSE APP");
    }
    
}
