package com.gitlab.tplsofteng.gamecorral.Entities;

//~--- JDK imports ------------------------------------------------------------

import java.io.Serializable;
import java.util.HashMap;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@XmlRootElement
@NamedQuery(
    name  = "findGameIdByTitle",
    query = "SELECT e.gameid " + "FROM GamePackage e " + "WHERE e.title =:title "
)
public class GamePackage implements Serializable {
    @Lob
    @Column(name = "gamefile")
    private byte[]       gameFile;
    @Lob
    @Column(name = "gamestorage")
    private HashMap<String,byte[]> gameStorage;
    @Id
    @GeneratedValue
    private long         gameid;
    @Lob
    @Column(name = "pagedata")
    private String       pageData;
    @Lob
    @Column(name = "gamethumb")
    private byte[]       thumb;
    @Column(name = "title")
    private String       title;
    @ManyToOne
    @JoinColumn(
        name     = "USER_ID",
        nullable = false
    )
    private GameSiteUser uploader;

    public GamePackage() {
        title = "gamesite-default";
    }

    /**
     * @return the gameid
     */
    public long getGameid() {
        return gameid;
    }

    /**
     * @param gameid the gameid to set
     */
    public void setGameid(long gameid) {
        this.gameid = gameid;
    }

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return the gameFile
     */
    public byte[] getGameFile() {
        return gameFile;
    }

    /**
     * @param gameFile the gameFile to set
     */
    public void setGameFile(byte[] gameFile) {
        this.gameFile = gameFile;
    }

    /**
     * @return the uploader
     */
    public GameSiteUser getUploader() {
        return uploader;
    }

    /**
     * @param uploader the uploader to set
     */
    public void setUploader(GameSiteUser uploader) {
        this.uploader = uploader;
    }

    /**
     * @return the thumb
     */
    public byte[] getThumb() {
        return thumb;
    }

    /**
     * @param thumb the thumb to set
     */
    public void setThumb(byte[] thumb) 
    {
        this.thumb = thumb;
    }

    /**
     * @return the pageData
     */
    public String getPageData() {
        return pageData;
    }

    /**
     * @param pageData the pageData to set
     */
    public void setPageData(String pageData) {
        this.pageData = pageData;
    }

    /**
     * @return the gameStorage
     */
    public HashMap<String,byte[]> getGameStorage() {
        return gameStorage;
    }

    /**
     * @param gameStorage the gameStorage to set
     */
    public void setGameStorage(HashMap<String,byte[]> gameStorage) {
        this.gameStorage = gameStorage;
    }
    
    public void insertStorageElement(String key, byte[] value)
    {
        this.gameStorage.put(key, value);
    }

}


//~ Formatted by Jindent --- http://www.jindent.com
