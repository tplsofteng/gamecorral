/*
 * To change this template, choose Tools | Templates and open the template in
 * the editor.
 */
package com.gitlab.tplsofteng.gamecorral.Entities;

import gameAPI.DataResponse;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import com.gitlab.tplsofteng.gamecorral.gameAPI.DataRequest;
import com.gitlab.tplsofteng.gamecorral.services.HashMapAdapter;
import com.gitlab.tplsofteng.gamecorral.services.JaxBHashmap;

/**
 * REST Web Service
 *
 * @author tplsofteng@gmail.com
 */
@Path("Persistence")
public class PersistenceResource {

    @Context
    private UriInfo context;

    /** Creates a new instance of GenericResource */
    public PersistenceResource() {
    }

    /**
     * Retrieves representation of an instance of Entities.GenericResource
     * @return an instance of java.lang.String
     */
//    @GET
//    @Produces("application/xml")
//    public JaxBHashmap getDataPair() 
//    {
//        HashMap<String, String> map = new HashMap<String,String>();
//        map.put("key", "val");
//        map.put("key1", "val1");
//        map.put("key2", "va2l");
//        HashMapAdapter adapter = new HashMapAdapter();
//        JaxBHashmap xMap = null;
//        try 
//        {
//            xMap  = adapter.marshal(map);
//        } 
//        
//        catch (Exception ex) 
//        {
//            Logger.getLogger(PersistenceResource.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        
//        return xMap;
//        
//    }
    
    @GET
    @Produces("application/xml")
    public DataRequest getDataPair() 
    {
        DataRequest r = new DataRequest();
        r.gameName = "thegame";
        r.requestType = "thetype";
        JaxBHashmap mapp = new JaxBHashmap();
        mapp.addPair("thekey", "theval");
        return r;
    }
        

    /**
     * PUT method for updating or creating an instance of GenericResource
     * @param content representation for the resource
     * @return an HTTP response with content of the updated or created resource.
     */
    @PUT
    @Consumes("application/xml")
    @Produces("application/xml")
    public DataResponse putXml(@Context HttpServletRequest req, JaxBHashmap map) {
        DataResponse response = new DataResponse();
        Iterator itx =req.getParameterMap().entrySet().iterator();
        while(itx.hasNext())
        {
             Entry e = (Entry)itx.next();
             System.out.println("Got Param"+ e.getKey()  +" val " + e.getValue());
        }
        HashMap<String,String> xMap = null;
        HashMapAdapter adapter = new HashMapAdapter();
        try 
        {
            xMap = adapter.unmarshal(map);
            Iterator it = xMap.entrySet().iterator();
            while(it.hasNext())
            {
                 Entry pairs = (Entry) it.next();
                 System.out.println("Got Pair"+ pairs.getKey()  +" val " + pairs.getValue()); 
            }
            
            response.setData("OK DATA SAVED");
              
        } 
        catch (Exception ex) 
        {
            Logger.getLogger(PersistenceResource.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("Error");
        }
        return response;
    }
}
