package com.gitlab.tplsofteng.gamecorral.Entities;

//~--- JDK imports ------------------------------------------------------------

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.*;

@NamedQuery(
    name              = "findUserIdByName",
    query             = "SELECT e.userId " + "FROM GameSiteUser e " + "WHERE e.userName =:userName "
)
@Entity
@Table(
    name              = "GameSiteUser",
    uniqueConstraints = { @UniqueConstraint(columnNames = { "userid", "username" }) }
)
public class GameSiteUser implements Serializable {
    @Column
    private int                points = 0;
    @ManyToMany
    @ElementCollection
    @JoinColumn(name = "userid")
    private List<GameSiteUser> friends;
    
    @ElementCollection
    private List<Long> friendRequests;

//  @OneToMany
//  @ElementCollection
//  @JoinColumn(name="gameid", referencedColumnName="userid")
    @OneToMany(mappedBy = "uploader")
    private List<GamePackage> Games;
    @Lob
    @Column(name = "avatar")
    private byte[]            avatar;
    @Column
    private String            password;

    // Member Variables
    @Id
    @GeneratedValue
    private long   userId;
    @Column(
        unique   = true,
        nullable = false
    )
    private String userName;

    // Getters & Setters

    public void setId(long id) {
        this.setUserId(id);
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void addGame(GamePackage game) {
        if (!this.Games.contains(game)) {
            this.getGames().add(game);
        }
    }

    public void removeGame(GamePackage game) {
        if (!this.Games.contains(game)) {
            this.getGames().remove(game);
        }
    }

    /**
     * @return the points
     */
    public int getPoints() {
        return points;
    }

    /**
     * @param points the points to set
     */
    public void setPoints(int points) {
        this.points = points;
    }

    /**
     * @return the userId
     */
    public long getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(long userId) {
        this.userId = userId;
    }

    /**
     * @return the Games
     */
    public List<GamePackage> getGames() {
        return Games;
    }

    /**
     * @param Games the Games to set
     */
    public void setGames(ArrayList<GamePackage> Games) {
        this.Games = Games;
    }

    /**
     * @return the Friends
     */
    public List<GameSiteUser> getFriends() {
        return friends;
    }

    /**
     * @param Friends the Friends to set
     */
    public void setFriends(ArrayList<GameSiteUser> Friends) {
        this.friends = Friends;
    }

    /**
     * @return the avatar
     */
    public byte[] getAvatar() {
        return avatar;
    }

    /**
     * @param avatar the avatar to set
     */
    public void setAvatar(byte[] avatar) {
        this.avatar = avatar;
    }

    /**
     * @return the friendRequests
     */
    public List<Long> getFriendRequests() {
        return friendRequests;
    }

    /**
     * @param friendRequests the friendRequests to set
     */
    public void setFriendRequests(ArrayList<Long> friendRequests) {
        this.friendRequests = friendRequests;
    }
    
    public void addFriendRequest(long friendRequesterId)
    {
        System.out.println("Adding " + friendRequesterId + " to request List for " + this.userName);
        this.friendRequests.add(friendRequesterId);
        System.out.println(this.userName + "'s account contains " + friendRequests.size() + " friends");
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
